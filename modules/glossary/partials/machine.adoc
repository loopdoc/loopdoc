*Machine* is the equipment entity which is hosting the process and the tool.
In ISA-88 it is referred as _Unit_.
