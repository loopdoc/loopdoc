A *reference variable* is
the input variable to a comparing element in a controlling system,
which sets the desired value of the controlled variable and is deducted from the command variable. +
http://www.electropedia.org/iev/iev.nsf/display?openform&ievref=351-48-02[IEV 351-48-02]
