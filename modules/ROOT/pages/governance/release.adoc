= Release Governance

== Planning

Release planning is here https://gitlab.com/loopdoc/loopdoc/-/releases.

== Versioning

Loopdoc follows the https://semver.org/[semantic versioning rules].
Each component release is versioned *major.minor.patch*.

Major::
Major releases occur when there are substantial changes in functionality or when new functionality breaks backwards compatibility.
Releases within the same major release line will maintain API compatibility.

Minor::
Minor releases add new features, improvements to existing features, and fixes that maintain backwards compatibility.

Patch::
Patch releases fix bugs and maintain backwards compatibility.
Patch releases happen as needed depending on the urgency of the fix.

Prerelease::
Major and minor releases may include prerelease versions (major.minor.patch-alpha.n | -beta.n | -rc.n).
Prerelease versions will be tagged as _next_ so that the npm client doesn't prefer a prerelease over a stable version.
Once a release candidate (rc) has been thoroughly tested, the stable release will be published.
In stable links from outside the antora-built docs.loopdoc, prerelease versions are referenced as _next_.

Latest version::
In stable links from outside the antora-built docs.loopdoc, prerelease versions are referenced as _latest_.
