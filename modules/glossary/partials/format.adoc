*Format* defines the structure of the value of a xref:concept:system/property_concept[property].
It rules the property type and the way its displayed.
