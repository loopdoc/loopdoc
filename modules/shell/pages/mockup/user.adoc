= Shell Mockup: User

:source-language: shell

include::partial$mockup_disclaimer.adoc[]

== User

=== Login

Assume loopdoc is running
[source]
----
loopdoc $ login bobby            <1>
    Please enter password for bobby: ****** ... Password is accepted
    "bobby" is logged in as administrator
bobby@loopdoc $                  <2>
----
<1> The `login` command is optional.
Any username can be used to login.
<2> Loopdoc prompt with logged in user

// == Edit user
